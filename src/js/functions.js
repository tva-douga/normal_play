import $ from 'jquery';

const setViewport = function() {
    const breakPoint = 768; // PC or SP の切り替えポイント(px)
    const pcWidth = 1280; // PCの最小幅(px)
    const ua = navigator.userAgent.toLowerCase();
    const isiOS = ua.indexOf('iphone') > -1 || ua.indexOf('ipod') > -1 || ua.indexOf('ipad') > -1;
    const iOSviewportW = isiOS ? document.documentElement.clientWidth : 0;

    function updateMetaViewport() {
        let deviceWidth = isiOS ? iOSviewportW : window.outerWidth;
        const viewportContent =
            deviceWidth >= breakPoint && deviceWidth < pcWidth
                ? 'width=' + pcWidth
                : 'width=device-width, initial-scale=1.0';
        document.querySelector('meta[name="viewport"]').setAttribute('content', viewportContent);
    }

    //イベントハンドラ登録
    window.addEventListener('resize', updateMetaViewport, false);
    window.addEventListener('orientationchange', updateMetaViewport, false);

    //初回イベント強制発動
    const ev = document.createEvent('UIEvent');
    ev.initEvent('resize', true, true);
    window.dispatchEvent(ev);
};
setViewport();

// top page : modal
$(document)
    .on('click', '.js-overview-open', e => {
        $(e.target)
            .siblings('.js-overview-target')
            .addClass('is-active');
        $('html').attr('style', 'overflow: hidden height: 100%');
    })
    .on('click', '.js-overview-close', e => {
        $(e.target)
            .parent('.js-overview-target')
            .removeClass('is-active');
        $('html').attr('style', '');
    });

$(document).on('click', '.js-sp-tab', e => {
    e.preventDefault();
    $('.js-sp-contents').removeClass('is-active');
    $($(e.currentTarget).data('target')).addClass('is-active');
    $('.js-sp-tab').removeClass('is-active');
    $(e.currentTarget).toggleClass('is-active');
});
