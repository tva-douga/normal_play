require('es6-promise').polyfill();

import Vue from 'vue';
import sanitizeHTML from 'sanitize-html';
import axios from 'axios';
import twemoji from 'twemoji'; // twitter emoji

import './functions';

const dataType = document.body.dataset.type;
const pathRelative = document.body.dataset.relative;

Vue.prototype.$sanitize = sanitizeHTML;

new Vue({
    el: '#app',
    data: {
        response: {},
        header: {},
        footer: {},
        twitterData: [],
        fetchStatus: true,
        isVoted: false,
        selectedRadio: null,
        isAccordionOpen: false,
    },
    mounted() {
        /** 各ページのpageTypeに合わせたjsonを取得 */
        if (dataType) {
            axios
                .get(`${pathRelative}data/${dataType}.json?time=${this.getTimeStamp()}`)
                .then(
                    function(res) {
                        this.response = res.data;
                    }.bind(this),
                )
                .then(
                    function(res) {
                        if (this.response.tweet && this.response.tweet.tweetListFileName) {
                            const tweetListFileName = `${pathRelative}data/${this.response.tweet.tweetListFileName}`;

                            this.getTweet(`${tweetListFileName}?time=${this.getTimeStamp()}`);

                            const getJsonLoop = setInterval(() => {
                                if (this.fetchStatus === false) {
                                    clearInterval(getJsonLoop);
                                    return false;
                                }
                                this.getTweet(`${tweetListFileName}?time=${this.getTimeStamp()}`);
                            }, 20000);
                        }
                    }.bind(this),
                )
                .catch(function(err) {
                    console.error(err);
                });
        }

        /** header.jsonの取得 */
        axios
            .get(`${pathRelative}data/header.json?time=${this.getTimeStamp()}`)
            .then(
                function(res) {
                    this.header = res.data;
                }.bind(this),
            )
            .catch(
                function(err) {
                    console.error(err);
                }.bind(this),
            );

        /** footer.jsonの取得 */
        axios
            .get(`${pathRelative}data/footer.json?time=${this.getTimeStamp()}`)
            .then(
                function(res) {
                    this.footer = res.data;
                }.bind(this),
            )
            .catch(
                function(err) {
                    console.error(err);
                }.bind(this),
            );
    },
    methods: {
        /*
         * Tweetを取得
         */
        getTweet(tweetListFileName) {
            axios(tweetListFileName)
                .then(
                    function(res) {
                        this.twitterData = res.data;
                        this.twitterData.tweet_list = this.twitterData.tweet_list.map(tweet => {
                            tweet.text = encodeURI(
                                twemoji.parse(
                                    tweet.text.replace(
                                        /([#＃][Ａ-Ｚａ-ｚA-Za-z一-鿆0-9０-９ぁ-ヶｦ-ﾟー._-]+)/g,
                                        '<em>$1</em>',
                                    ),
                                ),
                            );
                            return tweet;
                        });
                    }.bind(this),
                )
                .catch(
                    function(err) {
                        this.twitterData = [];
                        this.fetchStatus = false;
                        console.error(err);
                    }.bind(this),
                );
        },

        /*
         * タイムスタンプを返却
         */
        getTimeStamp() {
            const date = new Date();
            const timeStamp = `${date.getFullYear()}${('0' + (date.getMonth() + 1)).slice(-2)}${(
                '0' + date.getDate()
            ).slice(-2)}${('0' + date.getHours()).slice(-2)}${('0' + date.getMinutes()).slice(-2)}${(
                '0' + date.getSeconds()
            ).slice(-2)}`;
            return timeStamp;
        },

        /*
         * 送信後アクション（送信APIは/apps/douga/gifting/js/vote.js）
         */
        onSubmit(e) {
            e.preventDefault(); // submitイベント中止

            this.isVoted = true; // 投票フラグを変更

            // ラジオボタンをdisabled
            const radio = document.querySelectorAll('.c-radio1');
            if (radio) {
                radio.forEach(el => {
                    el.disabled = true;
                });
            }
        },
    },
});
