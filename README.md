# Welcome to Template Repository👋
リニューアル後のテンプレートリポジトリです。

## 運用方法
- 案件ごとにこのリポジトリを複製して作業を進めてください
- `package.json` に記述されている案件ごとのディレクトリを更新する( `/douga_mv/renewal_test/normal_play/` の部分を一括置換 )
- 各ejsファイルのmeta情報を更新
- 各ejsファイルの `path.absolute` と `path.relative` をアップロードするディレクトリに変更
※`relative` の値は `/douga_mv/` から記述
- 開発用ファイルはdistに生成され、公開用ファイルはprodに生成されます
- 個別に仕様変更する改修は対象案件のブランチで行い、システムの仕様変更する改修はmasterで行う

```
// 開発の開始（dist）
npm run watch:all

// 公開用ファイルの生成(開発用の出力をしてから実行する)
npm run prepare:all（prod）
```

prodディレクトリの内容を公開してください。

> このシステムは表示とデータを分離するために作成されました。
> フロントはVue、データはJSONで実装されています。
> データ(JSON)の仕様については[こちら](https://docs.google.com/spreadsheets/d/1nylN5OXkfsdU8d6NhXyUaUjPneSa4evDnWajjTHf6_g/edit#gid=0)を確認してください。

# システムを変更する際は以下を参考にしてください
## 全体の要約
- `ejs` でページを生成
- ページのデータは `json` で管理
- `script.js` 内でデータを取得し、出力には `Vue` を使用

## メインディレクトリについて
| メイン | 用途 |
|:--|:--|
| src  | 作業 |
| dist | 開発 |
| prod | 公開 |

## 作業ディレクトリについて
| 作業ディレクトリ | 内容 |
|:--|:--|
| src/data | ページ毎のデータをjsonファイルで管理 |
| src/ejs | この配下がディレクトリ構成になる |
| src/images | 画像用ディレクトリ |
| src/scss | scss用ディレクトリ |
| src/js | 処理用js + 出力用Vue |

| Vue用のjsファイル | 内容 |
|:--|:--|
| src/js/templates | ページを構成するhtmlの出力に利用するVue用のjsファイル |
| src/js/components | テンプレート内で使用するVue用のjsファイル |

## ページのデータ出力について
案件にあわせて修正してください。

|ファイル名|ページ|
|:--|:--|
|index.ejs|トップページ|
|error.ejs|エラーページ|
|free.ejs|無料の視聴ページ|
|play_{id}.ejs|有料の視聴ページ|

各ejsファイルの `data.type` がページを構成するHTML出力と紐付きます。
例えばトップページのejsでは以下のように設定します。
```ejs
<%
var data = {
        type: 'top',
        ...
}
%>
```

各ejsファイルで設定した `data.type` は `<body>` にある `data-type` 属性の値として付与されます。
この値を取得し、一致した内容が出力に使用するデータとして利用されます。
上記トップページの設定を元に行われる処理は以下です。
```js
// typeごとに内容をreturnし、HTML出力に使用するデータとして利用される
const importType = (type, state) => {
    switch(type) {
        case 'top':
            return { json: 'top.json', template: TopPage }
        default:
            return state
    }
}
```

あとは `Dive into code` です👋
